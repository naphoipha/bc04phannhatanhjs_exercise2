// Exercise 1
/**
 * /**
 * input: 
 * a = số ngày làm của nhân viên
 * const b = 100000
 * 
 * todo:
 * lương nhân viên = a x b
 * 
 * output:
 * lương nhân viên 
 */
function calSalary(){
    // console.log("yes");

    var dayEl = document.getElementById("days").value*1;
    var payDay = document.getElementById("payPerday").value*1

    var payEl = dayEl*payDay;
    document.getElementById("salResult").innerHTML = payEl;

    // console.log({dayEl,payDay,payEl});   
}

 //Exercise 2
 /**
  * input: 5 số thực a,b,c,d,e
  * 
  * todo: giá trị trung bình = (a+b+c+d+e)/5
  *
  * output: giá trị trung bình 
  */

 function calAverage() {
    var num1El = document.getElementById("num1").value*1;
    var num2El = document.getElementById("num2").value*1;
    var num3El = document.getElementById("num3").value*1;
    var num4El = document.getElementById("num4").value*1;
    var num5El = document.getElementById("num5").value*1;
    
    var average = (num1El+num2El+num3El+num4El+num5El)/5;
    document.getElementById("avResult").innerHTML = average;
 }
 
  //Exercise 3
  /**
  * input: 
  * a = số tiền USD
  * 1 USD = 23.500
  * const b = 23.500
  * 
  * todo:
  * quy đổi ra VND:
  * a*b
  * 
  * output:
  * số tiền VND
  */

function calExchange() {
    // console.log("yes");
    var moneyEl = document.getElementById("money").value*1;
    const usdmoney = 23500;

    // console.log({moneyEl,usdmoney});

    var exChange = usdmoney*moneyEl;

    // console.log({exChange});

    document.getElementById("exResult").innerHTML = exChange;
}
 
 //Exercise 4
 /**
  * input: 
  * a = chiều dài
  * b = chiều rộng
  * 
  * todo:
  * chu vi hình chữ nhật = (a+b)*2
  * diện tích = a*b
  * 
  * output:
  * chu vi hình chữ nhật và diện tích ra console log
  * 
  */
 function calRec(){
    var widthEl = document.getElementById("width").value*1;
    var heightEl = document.getElementById("height").value*1;

    var perEl = (widthEl+heightEl)/2;
    var arEl = widthEl*heightEl;

    document.getElementById("perResult").innerHTML = perEl;
    document.getElementById("arResult").innerHTML = arEl;
 }
 
  //Exercise 5
  /**
  * input:
  * nhập 2 ký số 
  * a = 12
  * 
  * todo:
  * tổng 2 ký số : 1 + 2 = 3;
  * lấy hàng đơn vị: a % 10
  * lấy hàng chục: a / 10
  * 
  * output:
  * xuất ra tổng 2 ký số ra console log
  * 
  * 
  */
function calSum() {
    var numEl = document.getElementById("numSpec").value*1;
    var firstDigit = Math.floor(numEl/10);
    var secondDigit = numEl % 10;
    
    var sumofnum = firstDigit + secondDigit;
    document.getElementById("numResult").innerHTML = sumofnum;
 }

 